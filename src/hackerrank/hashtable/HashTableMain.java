
package hackerrank.hashtable;

public class HashTableMain {
    public void makeSolution(){
        GroupingDishes groupingDishes = new GroupingDishes();
        //groupingDishes.go();
        
        AreFollowingPatterns areFollowingPatterns = new AreFollowingPatterns();
        //areFollowingPatterns.go();
        
        ContainsCloseNums containsCloseNums = new ContainsCloseNums();
        containsCloseNums.go();
    }
}
