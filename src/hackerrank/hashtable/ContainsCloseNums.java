package hackerrank.hashtable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContainsCloseNums {

    public void go() {
        int[] nums = {0, 1, 2, 3, 5, 2};
        int k = 2;
        System.out.println(containsCloseNums(nums, k));
    }

    boolean containsCloseNums(int[] nums, int k) {

        Map<Integer, List<Integer>> map = new HashMap<>();
        for(int i=0;i<nums.length;i++){
            List<Integer> list;
            if(map.containsKey(nums[i])){
                list = map.get(nums[i]);
            }else{
                list = new ArrayList<>();
            }
            list.add(i);
            map.put(nums[i], list);
        }
        for(int key: map.keySet()){
            List<Integer> list = map.get(key);
            if(isClose(list,k)){
                return true;
            }
        }
        return false;
    }
    boolean isClose(List<Integer> list, int k){
        for(int i=0; i<list.size()-1;i++){
            if(Math.abs(list.get(i)-list.get(i+1))<=k)
                return true;
        }
        return false;
    }
}
