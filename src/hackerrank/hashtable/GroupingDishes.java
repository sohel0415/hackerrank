package hackerrank.hashtable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class GroupingDishes {

    public void go() {
        String[][] dishes = {
            {"Pasta", "Tomato Sauce", "Onions", "Garlic"},
            {"Chicken Curry", "Chicken", "Curry Sauce"},
            {"Fried Rice", "Rice", "Onions", "Nuts"},
            {"Salad", "Spinach", "Nuts"},
            {"Sandwich", "Cheese", "Bread"},
            {"Quesadilla", "Chicken", "Cheese"}};

        String[][] d = groupingDishes(dishes);
    }

    String[][] groupingDishes(String[][] dishes) {

        HashMap<String, List<String>> hashMap = new HashMap<>();
        for (int i = 0; i < dishes.length; i++) {
            for (int j = 1; j < dishes[i].length; j++) {
                List<String> list;
                if (!hashMap.containsKey(dishes[i][j])) {
                    list = new ArrayList<>();
                } else {
                    list = hashMap.get(dishes[i][j]);
                }
                list.add(dishes[i][0]);
                hashMap.put(dishes[i][j], list);
            }
        }
        TreeMap<String, List<String>> treeMap = new TreeMap<>(hashMap);
        List<List<String>> relist = new ArrayList<>();
        for (String key : treeMap.keySet()) {
            List<String> list = treeMap.get(key);
            if (list.size() >= 2) {
                Collections.sort(list);
                list.add(0, key);
                relist.add(list);
            }
        }
        String[][] array = new String[relist.size()][];
        for (int i = 0; i < relist.size(); i++) {
            List<String> row = relist.get(i);
            array[i] = row.toArray(new String[row.size()]);
        }

        return array;
    }
}
