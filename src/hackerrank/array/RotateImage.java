/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hackerrank.array;

/**
 *
 * @author sohel0415
 */
public class RotateImage {

    public void go() {
        int[][] a = {{1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}};
        a = rotateImage(a);
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.print("\n");
        }
    }

    int[][] rotateImage(int[][] a) {

        int length = a.length;
        int lastIndex = length - 1;
        int i = 0;
        while (i <= length/2) {
            for (int j = i; j <= lastIndex - 1 - i; j++) {
                int temp1 = a[i][j];
                int temp2 = a[j][lastIndex - i];
                int temp3 = a[lastIndex - i][lastIndex - j];
                int temp4 = a[lastIndex - j][i];

                a[j][lastIndex - i] = temp1;
                a[lastIndex - i][lastIndex - j] = temp2;
                a[lastIndex - j][i] = temp3;
                a[i][j] = temp4;
            }
            i++;
        }
        return a;
    }
}
