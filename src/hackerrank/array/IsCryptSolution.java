package hackerrank.array;

import java.util.HashMap;

public class IsCryptSolution {

    public void go() {
        String[] crypt = {"AA", "AA", "AA"};
        char[][] solution = {
            {'A', '0'},
//            {'M', '1'},
//            {'Y', '2'},
//            {'E', '5'},
//            {'N', '6'},
//            {'D', '7'},
//            {'R', '8'},
//            {'S', '9'}
        };

        System.out.println(isCryptSolution(crypt, solution));

    }

    boolean isCryptSolution(String[] crypt, char[][] solution) {
        HashMap<Character, Character> hash = new HashMap<>();
        for (int i = 0; i < solution.length; i++) {
            hash.put(solution[i][0], solution[i][1]);
        }

        for (int i = 0; i < 3; i++) {
            if (crypt[i].length()>1&&hash.get(crypt[i].charAt(0)) == '0') {
                return false;
            }
        }

        String[] result = {"", "", ""};
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < crypt[i].length(); j++) {
                result[i] += hash.get(crypt[i].charAt(j));
            }
        }
        if (Double.parseDouble(result[0]) + Double.parseDouble(result[1]) != Double.parseDouble(result[2])) {
            return false;
        }
        return true;
    }

}
