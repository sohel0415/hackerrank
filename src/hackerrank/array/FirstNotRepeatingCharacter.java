package hackerrank.array;

import java.util.HashMap;

public class FirstNotRepeatingCharacter {

    public void go() {
        String s = "abacabad";
        System.out.print(firstNotRepeatingCharacter(s));
    }

    char firstNotRepeatingCharacter(String s) {
        int length = s.length();
        if(length==1)
            return s.charAt(0);
        HashMap<Character, Integer> hash = new HashMap<>();
        for(int i=0;i<length;i++){
            hash.put(s.charAt(i), (hash.getOrDefault(s.charAt(i), 0))+1);
        }
        for(int i=0;i<length;i++){
            if(hash.getOrDefault(s.charAt(i), 0)==1)
                return s.charAt(i);
        }
        return '-';
    }
}
