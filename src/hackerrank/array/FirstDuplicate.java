package hackerrank.array;

import java.util.HashMap;

public class FirstDuplicate {

    public void go() {
        int[] a = {2, 3, 3, 1, 5, 2};
        System.out.print(firstDuplicate(a));
    }

    int firstDuplicate(int[] a) {

        HashMap<Integer, Integer> hashMap = new HashMap<>();
        int length = a.length;
        for (int i = 0; i < length; i++) {
               if(hashMap.containsKey(a[i])){
                   return a[i];
               }
               hashMap.put(a[i], 1);
        }
        return -1;
    }
}
