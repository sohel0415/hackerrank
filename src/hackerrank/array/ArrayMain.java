
package hackerrank.array;

public class ArrayMain {
    
    public void makeSolution(){
        FirstDuplicate firstDuplicate = new FirstDuplicate();
        //firstDuplicate.go();
        
        FirstNotRepeatingCharacter character = new FirstNotRepeatingCharacter();
        //character.go();
        
        RotateImage rotateImage  = new RotateImage();
        //rotateImage.go();
        
        Sudoku2 sudoku2 = new Sudoku2();
        //sudoku2.go();
        
        IsCryptSolution isCryptSolution = new IsCryptSolution();
        isCryptSolution.go();
    }
}
