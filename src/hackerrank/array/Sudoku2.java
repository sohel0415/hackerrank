package hackerrank.array;

import java.util.HashMap;

public class Sudoku2 {

    public void go() {
        char[][] grid
                = {{'.', '.', '.', '1', '4', '.', '.', '2', '.'},
                {'.', '.', '6', '.', '.', '.', '.', '.', '.'},
                {'.', '.', '.', '.', '.', '.', '.', '.', '.'},
                {'.', '.', '1', '.', '.', '.', '.', '.', '.'},
                {'.', '6', '7', '.', '.', '.', '.', '.', '9'},
                {'.', '.', '.', '.', '.', '.', '8', '1', '.'},
                {'.', '3', '.', '.', '.', '.', '.', '.', '6'},
                {'.', '.', '.', '.', '.', '7', '.', '.', '.'},
                {'.', '.', '.', '5', '.', '.', '6', '7', '.'}
                };
        System.out.println(sudoku2(grid));

    }

    boolean sudoku2(char[][] grid) {

        for (int i = 0; i < 9; i++) {
            HashMap<Character, Boolean> hash = new HashMap<>();
            for (int j = 0; j < 9; j++) {
                if (grid[i][j] != '.' && hash.getOrDefault(grid[i][j], false) == true) {
                    return false;
                }

                hash.put(grid[i][j], true);
            }
        }
        for (int i = 0; i < 9; i++) {
            HashMap<Character, Boolean> hash = new HashMap<>();
            for (int j = 0; j < 9; j++) {
                if (grid[j][i] != '.' && hash.getOrDefault(grid[j][i], false) == true) {
                    return false;
                }

                hash.put(grid[j][i], true);
            }
        }
        for (int i = 0; i < 9; i +=3) {
            for (int j = 0; j < 9; j +=3) {
                if (isSubMatrixTrue(grid, i, j) == false) {
                    return false;
                }
            }
        }
        return true;
    }

    boolean isSubMatrixTrue(char[][] grid, int i, int j) {
        HashMap<Character, Boolean> hash = new HashMap<>();
        for (int k = i; k < i + 3; k++) {
            for (int l = j; l < j + 3; l++) {
                if (grid[k][l] != '.' && hash.getOrDefault(grid[k][l], false) == true) {
                    return false;
                }

                hash.put(grid[k][l], true);
            }
        }
        
        return true;
    }
}
