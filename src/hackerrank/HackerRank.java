
package hackerrank;

import hackerrank.array.ArrayMain;
import hackerrank.hashtable.HashTableMain;
import hackerrank.linkedlist.LinkedListMain;
import java.util.concurrent.LinkedBlockingDeque;

public class HackerRank {

    public static void main(String[] args) {
        ArrayMain arrayMain = new ArrayMain();
        //arrayMain.makeSolution();
        
        LinkedListMain linkedListMain = new LinkedListMain();
        //linkedListMain.makeSolution();
        
        HashTableMain hashTableMain = new HashTableMain();
        hashTableMain.makeSolution();
    }
    
}
