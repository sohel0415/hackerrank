package hackerrank.linkedlist;

public class MergeTwoLinkedLists {

    class ListNode<T> {

        ListNode(T x) {
            value = x;
        }
        T value;
        ListNode<T> next;
    }

    public void go() {
        ListNode<Integer> l1 = new ListNode<>(1);
        ListNode<Integer> l2 = new ListNode<>(2);
        ListNode<Integer> l3 = new ListNode<>(3);
        ListNode<Integer> l4 = new ListNode<>(4);
        ListNode<Integer> l5 = new ListNode<>(5);
        ListNode<Integer> l6 = new ListNode<>(6);
        l1.next = l2;
        l2.next = l3;
//        l3.next = l4;
        l4.next = l5;
        l5.next = l6;

        ListNode<Integer> r = mergeTwoLinkedLists(l1, l4);
        while (r != null) {
            System.out.println(r.value);
            r = r.next;
        }

    }

    ListNode<Integer> mergeTwoLinkedLists(ListNode<Integer> l1, ListNode<Integer> l2) {
        ListNode<Integer> mergedList = new ListNode<>(0);
        ListNode<Integer> p = mergedList;
        while (l1 != null || l2 != null) {
            if (l1 != null && l2 != null) {
                if (l1.value <= l2.value) {
                    ListNode<Integer> temp = new ListNode<>(l1.value);
                    mergedList.next = temp;
                    mergedList = mergedList.next;
                    l1 = l1.next;
                }
                else if (l1.value > l2.value) {
                    ListNode<Integer> temp = new ListNode<>(l2.value);
                    mergedList.next = temp;
                    mergedList = mergedList.next;
                    l2 = l2.next;
                }
            } else if (l1 != null && l2 == null) {
                ListNode<Integer> temp = new ListNode<>(l1.value);
                mergedList.next = temp;
                mergedList = mergedList.next;
                l1 = l1.next;
            } else if (l2 != null && l1 == null) {
                ListNode<Integer> temp = new ListNode<>(l2.value);
                mergedList.next = temp;
                mergedList = mergedList.next;
                l2 = l2.next;
            }
        }
        return p.next;
    }
}
