
package hackerrank.linkedlist;

public class LinkedListMain {
     public void makeSolution(){
        RemoveKFromList removeKFromList = new RemoveKFromList();
        //removeKFromList.go();
        
        IsListPalindrome isListPalindrome = new IsListPalindrome();
        //ghyuisListPalindrome.go();
        
        AddTwoHugeNumbers addTwoHugeNumbers = new AddTwoHugeNumbers();
        //addTwoHugeNumbers.go();
        
        MergeTwoLinkedLists linkedLists = new MergeTwoLinkedLists();
        //linkedLists.go();
        
        ReverseNodesInKGroups reverseNodesInKGroups  = new ReverseNodesInKGroups();
        //reverseNodesInKGroups.go();
        
        RearrangeLastN rearrangeLastN = new RearrangeLastN();
        rearrangeLastN.go();
    }
}
