
package hackerrank.linkedlist;

import java.util.Stack;


public class IsListPalindrome {
    class ListNode<T> {

        ListNode(T x) {
            value = x;
        }
        T value;
        ListNode<T> next;
    }

    public void go() {

        ListNode<Integer> l1 = new ListNode<>(1);
        ListNode<Integer> l2 = new ListNode<>(4);
        ListNode<Integer> l3 = new ListNode<>(2);
        ListNode<Integer> l4 = new ListNode<>(4);
        ListNode<Integer> l5 = new ListNode<>(1);
        //ListNode<Integer> l6 = new ListNode<>(1);
        l1.next = l2;
        l2.next = l3;
        l3.next = l4;
        l4.next = l5;
        //l5.next = l6;

       
            System.out.println(isListPalindrome(l1));
        

    }

    boolean isListPalindrome(ListNode<Integer> l) {

        ListNode<Integer> head = l;
        ListNode<Integer> first = head;
        ListNode<Integer> second = head;
        
        while(first!=null&&first.next!=null){
            first = first.next.next;
            second = second.next;
        }
 
       second = reverse(second);
       
        while(second!=null){
            if(!second.value.equals(head.value))
                return false;
            
            second = second.next;
            head = head.next;
        }
        return true;
    }
    ListNode<Integer> reverse(ListNode<Integer> second){
         ListNode<Integer> prev = null;
        ListNode<Integer> current = second;
        ListNode<Integer> next;
        while (current != null) 
        {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        second = prev;
        
        return second;
    }
}
