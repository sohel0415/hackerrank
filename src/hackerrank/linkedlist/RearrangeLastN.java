package hackerrank.linkedlist;

public class RearrangeLastN {

    class ListNode<T> {

        ListNode(T x) {
            value = x;
        }
        T value;
        ListNode<T> next;
    }

    public void go() {

        ListNode<Integer> l1 = new ListNode<>(1);
        ListNode<Integer> l2 = new ListNode<>(2);
        ListNode<Integer> l3 = new ListNode<>(3);
        ListNode<Integer> l4 = new ListNode<>(2);
        ListNode<Integer> l5 = new ListNode<>(4);
        l1.next = l2;
        l2.next = l3;
        l3.next = l4;
        l4.next = l5;

        ListNode<Integer> r = rearrangeLastN(l1, 3);
        while (r != null) {
            System.out.println(r.value);
            r = r.next;
        }

    }

    ListNode<Integer> rearrangeLastN(ListNode<Integer> l, int n) {

        int count = 0;
        ListNode<Integer> checkCount = l;
        while (checkCount != null) {
            count++;
            checkCount = checkCount.next;
        }
        ListNode<Integer> first = new ListNode<>(0);
        ListNode<Integer> firstNodeToAddLast = first;
        ListNode<Integer> second = new ListNode<>(0);
        ListNode<Integer> secondToPrint = second;
        ListNode<Integer> traverseNode = l;

        while (count - n > 0) {
            count--;
            first.next = new ListNode<>(traverseNode.value);
            first = first.next;
            traverseNode = traverseNode.next;
        }
        while (traverseNode != null) {
            second.next = traverseNode;
            second = second.next;
            traverseNode = traverseNode.next;
        }

        second = insert(second, firstNodeToAddLast.next);
        return secondToPrint.next;
    }

    ListNode<Integer> insert(ListNode<Integer> result, ListNode<Integer> temp) {
        while (temp != null) {
            result.next = temp;
            result = result.next;
            temp = temp.next;
        }

        return result;
    }
}
