package hackerrank.linkedlist;

public class AddTwoHugeNumbers {

    class ListNode<T> {

        ListNode(T x) {
            value = x;
        }
        T value;
        ListNode<T> next;
    }

    public void go() {
        ListNode<Integer> l1 = new ListNode<>(9876);
        ListNode<Integer> l2 = new ListNode<>(5432);
        ListNode<Integer> l3 = new ListNode<>(1999);
        ListNode<Integer> l4 = new ListNode<>(1);
        ListNode<Integer> l5 = new ListNode<>(8001);
        //ListNode<Integer> l6 = new ListNode<>(1);
        l1.next = l2;
        l2.next = l3;
//        l3.next = l4;
        l4.next = l5;
        //l5.next = l6;

        ListNode<Integer> r = addTwoHugeNumbers(l1, l4);
         while (r != null) {
            System.out.println(r.value);
            r = r.next;
        }

    }

    ListNode<Integer> addTwoHugeNumbers(ListNode<Integer> a, ListNode<Integer> b) {
            a = reverse(a);
            b = reverse(b);
            int carrige = 0;
            ListNode<Integer> resultNode = new ListNode<>(0);
            ListNode<Integer> finalNode = resultNode;
            while(a!=null||b!=null){
                int i =0;
                int j=0;
                if(a!=null){
                    i = a.value;
                    a = a.next;
                }
                if(b!=null){
                    j = b.value;
                    b = b.next;
                }
                
                int result = i+j+carrige;
                carrige = result/10000;
                int digit = result%10000;
                
                ListNode<Integer> temp = new ListNode<>(digit);
                resultNode.next = temp;
                resultNode = resultNode.next;
                
            }
            if(carrige>0){
                  ListNode<Integer> temp = new ListNode<>(carrige);
                resultNode.next = temp;
                resultNode = resultNode.next;
            }
              ListNode<Integer> t = reverse(finalNode.next);
            return t;
    }

    ListNode<Integer> reverse(ListNode<Integer> second) {
        ListNode<Integer> prev = null;
        ListNode<Integer> current = second;
        ListNode<Integer> next;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        second = prev;

        return second;
    }
}
