package hackerrank.linkedlist;

public class RemoveKFromList {

    class ListNode<T> {

        ListNode(T x) {
            value = x;
        }
        T value;
        ListNode<T> next;
    }

    public void go() {

        ListNode<Integer> l1 = new ListNode<>(1);
        ListNode<Integer> l2 = new ListNode<>(2);
//        ListNode<Integer> l3 = new ListNode<>(3);
//        ListNode<Integer> l4 = new ListNode<>(2);
//        ListNode<Integer> l5 = new ListNode<>(4);
        l1.next = l2;
//        l2.next = l3;
//        l3.next = l4;
//        l4.next = l5;

        ListNode<Integer> r = removeKFromList(l1, 1);
        while (r != null) {
            System.out.println(r.value);
            r = r.next;
        }

    }

    ListNode<Integer> removeKFromList(ListNode<Integer> l, int k) {

        if (l == null) {
            return null;
        }
        ListNode<Integer> head = l;
        while (l.value == k) {
            if (l.next == null) {
                return null;
            }
            head = l.next;
            l = l.next;
        }
        while (l.next != null) {
            if (l.next.value == k) {
                l.next = l.next.next;
            } else {
                l = l.next;
            }
        }
        return head;
    }

    ListNode<Integer> removeKFromListGoodSolution(ListNode<Integer> l, int k) {

        ListNode<Integer> helper = new ListNode(0);
        helper.next = l;
        ListNode<Integer> p = helper;

        while (p.next != null) {
            if (p.next.value == k) {
                ListNode next = p.next;
                p.next = next.next;
            } else {
                p = p.next;
            }
        }

        return helper.next;
    }
}
