package hackerrank.linkedlist;

public class ReverseNodesInKGroups {

    class ListNode<T> {

        ListNode(T x) {
            value = x;
        }
        T value;
        ListNode<T> next;
    }

    public void go() {

        ListNode<Integer> l1 = new ListNode<>(1);
        ListNode<Integer> l2 = new ListNode<>(2);
        ListNode<Integer> l3 = new ListNode<>(3);
        ListNode<Integer> l4 = new ListNode<>(2);
        ListNode<Integer> l5 = new ListNode<>(4);
        l1.next = l2;
        l2.next = l3;
        l3.next = l4;
        l4.next = l5;

        ListNode<Integer> r = reverseNodesInKGroups(l1, 3);
        while (r != null) {
            System.out.println(r.value);
            r = r.next;
        }

    }

    ListNode<Integer> reverseNodesInKGroups(ListNode<Integer> l, int k) {

        ListNode<Integer> result = new ListNode<>(0);
        ListNode<Integer> r = result;
        ListNode<Integer> temp = new ListNode<>(0);
        ListNode<Integer> head = temp;
        int count = 0;
        while(l!=null){
            count++;
            temp.next = new ListNode<>(l.value);
            temp = temp.next;
            l = l.next;           
            if(count%k==0){
                head = reverse(head.next);
                result = insert(result, head);
                temp = new ListNode<>(0);
                head = temp;
            }
        }
        result = insert(result, head.next);
        return r.next;
    }

    ListNode<Integer> insert(ListNode<Integer> result, ListNode<Integer> temp) {
        while (temp != null) {
            result.next = temp;
            result = result.next;
            temp = temp.next;
        }

        return result;
    }
    
    ListNode<Integer> reverse(ListNode<Integer> second) {
        ListNode<Integer> prev = null;
        ListNode<Integer> current = second;
        ListNode<Integer> next;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        second = prev;

        return second;
    }
}
